﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using YouProj.DependencyInjection.Base.Interfaces;

namespace YouProj.DependencyInjection.DryIoc.Containers
{
  public class DryIocContainerExtension : IContainerExtension<IContainer>
  {
    public IContainer Instance { get; }

    public DryIocContainerExtension(IContainer container)
    {
      Instance = container;
    }

    public void RegisterInstance(Type type, object instance)
    {
      Instance.UseInstance(type, instance);
    }

    public void RegisterSingleton(Type from, Type to)
    {
      Instance.Register(from, to, Reuse.Singleton,
        made: Made.Of(FactoryMethod.ConstructorWithResolvableArguments),
        ifAlreadyRegistered: IfAlreadyRegistered.Replace);
    }

    public void Register(Type from, Type to)
    {
      Instance.Register(from, to,
        made: Made.Of(FactoryMethod.ConstructorWithResolvableArguments),
        ifAlreadyRegistered: IfAlreadyRegistered.Replace);
    }

    public void Register(Type from, Type to, string name)
    {
      Instance.Register(from, to,
        made: Made.Of(FactoryMethod.ConstructorWithResolvableArguments),
        ifAlreadyRegistered: IfAlreadyRegistered.Replace,
        serviceKey: name);
    }

    public object Resolve(Type type)
    {
      return Instance.Resolve(type);
    }

    public object Resolve(Type type, string name)
    {
      return Instance.Resolve(type, serviceKey: name);
    }
  }
}
