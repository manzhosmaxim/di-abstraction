﻿using System;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using YouProj.DependencyInjection.Base.Interfaces;

namespace YouProj.DependencyInjection.Autofac.Containers
{
  public class AutofacContainerExtension : IContainerExtension<ContainerBuilder>
  {
    private IContainer _container;

    public ContainerBuilder Instance { get; }

    public AutofacContainerExtension(ContainerBuilder containerBuilder)
    {
      Instance = containerBuilder;
    }

    public object Resolve(Type type)
    {
      return _container.Resolve(type);
    }

    public object Resolve(Type type, string name)
    {
      return _container.ResolveNamed(name, type);
    }

    public void RegisterInstance(Type type, object instance)
    {
      Instance.RegisterInstance(instance).As(type);
    }

    public void RegisterSingleton(Type from, Type to)
    {
      Instance.RegisterType(to).As(from).SingleInstance();
    }

    public void RegisterSingletonWithFactory<TInterface, TImplementation>(Func<IContainerProvider, TImplementation> factory)
    {
      var from = typeof(TInterface);
      Instance.Register((c) => factory(this)).As(from).SingleInstance();
    }

    public void Register(Type from, Type to)
    {
      Instance.RegisterType(to).As(from);
    }

    public void Register(Type from, Type to, string name)
    {
      Instance.RegisterType(to).As(from).Named(name, to);
    }

    public void Build(ContainerBuildOptions options = ContainerBuildOptions.None)
    {
      _container = Instance.Build(options: options);
    }

    public AutofacWebApiDependencyResolver GetWebApiResolver()
    {
      return new AutofacWebApiDependencyResolver(_container);
    }

    public AutofacDependencyResolver GetMvcResolver()
    {
      return new AutofacDependencyResolver(_container);
    }
  }
}
