﻿using System;

namespace YouProj.DependencyInjection.Base.Interfaces
{
  public interface IContainerProvider
  {
    object Resolve(Type type);

    object Resolve(Type type, string name);
  }
}