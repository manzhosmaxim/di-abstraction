﻿namespace YouProj.DependencyInjection.Base.Interfaces
{
  public interface IContainerExtension<TContainer> : IContainerExtension
  {
    TContainer Instance { get; }
  }

  public interface IContainerExtension : IContainerProvider, IContainerRegistry
  {
  }
}