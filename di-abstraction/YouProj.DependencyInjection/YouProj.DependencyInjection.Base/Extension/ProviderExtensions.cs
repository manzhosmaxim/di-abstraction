﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouProj.DependencyInjection.Base.Interfaces;
using YouProj.DependencyInjection.Base.Models;

namespace YouProj.DependencyInjection.Base.Extension
{
  public static class ProviderExtensions
  {
    public static TInterface Resolve<TInterface>(this IContainerProvider container)
    {
      TInterface service = (TInterface)container.Resolve(typeof(TInterface));
      if (service == null)
      {
        throw new UnresolvedException(string.Format("Can not resolve implementation for service: {0}", typeof(TInterface).FullName));
      }
      return service;
    }
  }
}
