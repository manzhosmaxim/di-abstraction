﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouProj.DependencyInjection.Base.Interfaces;

namespace YouProj.DependencyInjection.Base.Extension
{
  public static class RegistryExtensions
  {
    public static IContainerRegistry Register<TInterface, TImplementation>(this IContainerRegistry container) where TImplementation : TInterface
    {
      container.Register(typeof(TInterface), typeof(TImplementation));
      return container;
    }

    public static IContainerRegistry RegisterInstance<TInterface>(this IContainerRegistry container, TInterface instance)
    {
      container.RegisterInstance(typeof(TInterface), instance);
      return container;
    }

    public static IContainerRegistry RegisterSingleton<TInterface, TImplementation>(this IContainerRegistry container) where TImplementation : TInterface
    {
      container.RegisterSingleton(typeof(TInterface), typeof(TImplementation));
      return container;
    }
  }
}
