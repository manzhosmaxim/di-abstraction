﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouProj.DependencyInjection.Base.Interfaces;
using YouProj.DependencyInjection.Base.Models;

namespace YouProj.DependencyInjection.Base.ServiceLocator
{
  public static class DependencyResolverProvider
  {
    private static IContainerExtension _containerExtension;
    
    public static void SetContainer(IContainerExtension container)
    {
      _containerExtension = container;
    }

    public static T GetService<T>()
    {
      var type = typeof(T);
      T service = (T)_containerExtension.Resolve(type);
      if (service == null)
      {
        throw new UnresolvedException(string.Format("Can not resolve implementation for service: {0}", typeof(T).FullName));
      }
      return service;
    }

    public static object GetService(Type type)
    {
      object service = _containerExtension.Resolve(type);
      if (service == null)
      {
        throw new UnresolvedException(string.Format("Can not resolve implementation for service: {0}", type.FullName));
      }
      return service;
    }
  }
}
