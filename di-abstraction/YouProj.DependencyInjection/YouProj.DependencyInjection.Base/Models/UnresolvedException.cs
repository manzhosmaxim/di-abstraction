﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace YouProj.DependencyInjection.Base.Models
{
  public class UnresolvedException : Exception
  {
    public UnresolvedException()
    {
    }

    public UnresolvedException(string message) : base(message)
    {
    }

    public UnresolvedException(string message, Exception innerException) : base(message, innerException)
    {
    }

    protected UnresolvedException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
  }
}
