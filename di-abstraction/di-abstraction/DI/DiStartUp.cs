﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using YouProj.DependencyInjection.Autofac.Containers;
using YouProj.DependencyInjection.Base.ServiceLocator;
using YouProj.Services.Common.DI;

namespace YouProj.WepApi.DI
{
  public class DiStartUp
  {
    public static void Setup(HttpConfiguration configuration)
    {
      var executingAssembly = Assembly.GetExecutingAssembly();

      //Autofac registrations 
      var containerBuilder = new ContainerBuilder();
      containerBuilder.RegisterFilterProvider();
      containerBuilder.RegisterWebApiFilterProvider(configuration);
      containerBuilder.RegisterSource(new ViewRegistrationSource());
      containerBuilder.RegisterControllers(executingAssembly);
      containerBuilder.RegisterApiControllers(executingAssembly);

      //Our Custom registration
      var containerExtension = new AutofacContainerExtension(containerBuilder);

      //Fluent Register fo needed modules
      containerExtension
        .RegisterServiceInjectionModule();

      // Call build, if DI container depend on it
      containerExtension.Build();

      var mvcResolver = containerExtension.GetMvcResolver();
      var apiResolver = containerExtension.GetWebApiResolver();
      System.Web.Mvc.DependencyResolver.SetResolver(mvcResolver);             // MVC
      GlobalConfiguration.Configuration.DependencyResolver = apiResolver;     // Web API
      DependencyResolverProvider.SetContainer(containerExtension);            // Service locator for Schedules, tests or something else
    }
  }
}