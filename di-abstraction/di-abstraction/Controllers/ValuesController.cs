﻿using System.Collections.Generic;
using System.Web.Http;
using YouProj.Services.Common.Interfaces;

namespace YouProj.WepApi.Controllers
{
  [Route("api/values")]
  public class ValuesController : ApiController
  {
    private readonly ISomeService _someService;

    public ValuesController(ISomeService someService)
    {
      _someService = someService;
    }

    [HttpGet]
    [Route]
    public string Get()
    {
      return _someService.GetSomeString();
    }
  }
}
