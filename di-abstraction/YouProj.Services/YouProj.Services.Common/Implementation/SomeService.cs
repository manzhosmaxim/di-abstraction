﻿using YouProj.Services.Common.Interfaces;

namespace YouProj.Services.Common.Implementation
{
  public class SomeService: ISomeService
  {
    private readonly string _someResult;

    public SomeService(SomeConfig someConfig)
    {
      _someResult = someConfig.SomeResult;
    }

    public string GetSomeString()
    {
      return _someResult;
    }
  }
}
