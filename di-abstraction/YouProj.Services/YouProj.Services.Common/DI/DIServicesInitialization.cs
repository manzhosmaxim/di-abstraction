﻿using YouProj.DependencyInjection.Base.Extension;
using YouProj.DependencyInjection.Base.Interfaces;
using YouProj.Services.Common.Implementation;
using YouProj.Services.Common.Interfaces;

namespace YouProj.Services.Common.DI
{
  public static class DiServicesInitialization
  {
    public static IContainerExtension RegisterServiceInjectionModule(this IContainerExtension container)
    {
      container
        .RegisterInstance<SomeConfig>(new SomeConfig())
        .Register<ISomeService,SomeService>();

      return container;
    }
  }
}
